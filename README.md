CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements


INTRODUCTION
------------

A challenge for current, and potential idfive Backend developers, to assess
knowledge and skills in Drupal custom module creation, as well as working
with configuration, coding, formatting, and documenting to standard. Use both
this documentation site of idfive preferences, and examples, as well as any
relevant drupal documentation desired to enact this module.

 * For a full description of the paragraphs module challenge:
   https://developers.idfive.com/#/challenges/drupal-paragraph-module


REQUIREMENTS
------------

Create a custom Drupal 8 module that accomplishes the following:

 * Interacts with the paragraphs module, to show static content.
 * Creates a paragraph bundle that includes, title, short body text, and
   Call to action link.
 * Allows an admin user to be able to create a paragraph entity of the
   module bundle type through the UI and place the paragraph on a node.
 * Conforms to current Drupal coding standards.
 * Includes all custom configuration.
 * Includes all needed documentation per drupal and idfive standards.
 * Passes WAVE AA level testing.
 * Passes W3C Validator testing.
